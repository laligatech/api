<?php

namespace App\DataFixtures;

use App\Entity\Clubes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ClubesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $club = new Clubes();
        $club->setNombre('Liverpool F. C.');
        $club->setEscudo('https://upload.wikimedia.org/wikipedia/commons/9/90/Liverpool_FC_logo_simplified.png');
        $club->setLimiteJugadores(5);
        $club->setLimiteSalarial(20000.00);
        $manager->persist($club);

        $club = new Clubes();
        $club->setNombre('Real Madrid C. F.');
        $club->setEscudo('https://2.bp.blogspot.com/-cF690wIpl04/Uc-p-jb39NI/AAAAAAAAAFQ/Hd5WTBXDgxE/s1600/logo+real+madrid.png');
        $club->setLimiteJugadores(5);
        $club->setLimiteSalarial(20000.00);
        $manager->persist($club);

        $club = new Clubes();
        $club->setNombre('F. C. Barcelona');
        $club->setEscudo('https://pngimage.net/wp-content/uploads/2018/05/escudo-barcelona-3d-png-3.png');
        $club->setLimiteJugadores(5);
        $club->setLimiteSalarial(20000.00);
        $manager->persist($club);

        $club = new Clubes();
        $club->setNombre('F. C. Bayern München');
        $club->setEscudo('https://i7.pngflow.com/pngimage/638/607/png-fc-bayern-munich-logo-emblem-graphics-football-emblem-trademark-logo-sticker-clipart.png');
        $club->setLimiteJugadores(5);
        $club->setLimiteSalarial(20000.00);
        $manager->persist($club);

        $club = new Clubes();
        $club->setNombre('C. Atlético de Madrid');
        $club->setEscudo('https://logodownload.org/wp-content/uploads/2017/02/atletico-Madrid-logo-escudo-2.png');
        $club->setLimiteJugadores(5);
        $club->setLimiteSalarial(20000.00);
        $manager->persist($club);

        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Perfiles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PerfilesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $perfil = new Perfiles();
        $perfil->setNombre('Jugador');
        $perfil->setHasLimitUser(true);
        $perfil->setHasPosicion(true);
        $perfil->setIsPagado(true);
        $manager->persist($perfil);

        $perfil = new Perfiles();
        $perfil->setNombre('Entrenador');
        $perfil->setHasLimitUser(true);
        $perfil->setHasPosicion(false);
        $perfil->setIsPagado(true);
        $manager->persist($perfil);

        $perfil = new Perfiles();
        $perfil->setNombre('Canterano');
        $manager->persist($perfil);
        $perfil->setHasLimitUser(false);
        $perfil->setHasPosicion(true);
        $perfil->setIsPagado(false);

        $manager->flush();
    }
}

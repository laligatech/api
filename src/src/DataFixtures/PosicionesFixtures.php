<?php

namespace App\DataFixtures;

use App\Entity\Posiciones;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PosicionesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $posicion = new Posiciones();
        $posicion->setNombre('Portero');
        $manager->persist($posicion);

        $posicion = new Posiciones();
        $posicion->setNombre('Defensa');
        $manager->persist($posicion);

        $posicion = new Posiciones();
        $posicion->setNombre('Centrocampista');
        $manager->persist($posicion);

        $posicion = new Posiciones();
        $posicion->setNombre('Delantero');
        $manager->persist($posicion);

        $manager->flush();
    }
}

<?php

namespace App\Repository;

use App\Entity\Perfiles;
use App\Entity\Posiciones;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @throws OptimisticLockException|ORMException|Exception
     */
    public function saveUser(User $user, $request, $tipo): User
    {
        $user->setNombre(trim($request->nombre));
//        $user->setEmail(rand().'@mail.com');
        $user->setEmail(trim($request->email));
        $user->setPerfil($this->_em->getRepository(Perfiles::class)->find($request->perfilId));
        if(isset($request->posicionId) && $request->posicionId){
            $user->setPosicion($this->_em->getRepository(Posiciones::class)->find($request->posicionId));
        }
        if ($request->fechaNacimiento) {
            if (1 == $tipo) {
                $fecha = explode('/', $request->fechaNacimiento);
                if (count($fecha) <= 1) {
                    $fecha = explode('-', $request->fechaNacimiento);
                }
                $fechaNacimiento = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
            } else {
                $fechaNacimiento = $request->fechaNacimiento;
            }

            $user->setFechaNacimiento(new \DateTime($fechaNacimiento));
        }
        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function usersByPerfilNotHired($perfil): array
    {
        $params = [
            ':perfil' => $perfil,
        ];
        $query = 'select DISTINCT (u.id), u.nombre
                    from `user` u 
                    where u.perfil_id = :perfil
                    and u.id not IN(select user_id from contratos c where user_id = u.id and activo = true)';

        return $this->getEntityManager()->getConnection()->executeQuery(\strtr($query, $params))->fetchAllAssociative();
    }
}

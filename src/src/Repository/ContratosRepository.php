<?php

namespace App\Repository;

use App\Entity\Clubes;
use App\Entity\Contratos;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contratos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contratos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contratos[]    findAll()
 * @method Contratos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContratosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contratos::class);
    }

    public function saveContract(Contratos $contrato, $request): Contratos
    {
        $contrato->setClub($this->_em->getRepository(Clubes::class)->find($request->club));
        $contrato->setUser($this->_em->getRepository(User::class)->find($request->userId));
        if ($request->salario) {
            $contrato->setSalario($request->salario);
        }
        $contrato->setActivo($request->activo);
        $this->_em->persist($contrato);
        $this->_em->flush();

        return $contrato;
    }

    public function limiteContratosPorUsuario($club)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.user', 'u')
            ->leftJoin('u.perfil', 'p')
            ->andWhere('c.club = :club')
            ->andWhere('p.hasLimitUser = true')
            ->andWhere('p.nombre = :perfil')
            ->andWhere('c.activo = true')
            ->setParameter('club', $club)
            ->setParameter('perfil', 'Jugador')
            ->distinct('c.id')
            ->getQuery()
            ->getResult()
            ;
    }

    public function limiteEntrenador($request): bool
    {
        $contrados = [];
        if ('Entrenador' == $request->perfil) {
            $contrados = $this->createQueryBuilder('c')
                ->leftJoin('c.user', 'u')
                ->leftJoin('u.perfil', 'p')
                ->andWhere('c.club = :club')
                ->andWhere('p.nombre = :perfil')
                ->andWhere('c.activo = true')
                ->setParameter('club', $request->club)
                ->setParameter('perfil', 'Entrenador')
                ->distinct('c.id')
                ->getQuery()
                ->getResult()
            ;
        }

        return count($contrados) >= 1 && $request->activo;
    }
}

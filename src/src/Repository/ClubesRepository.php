<?php

namespace App\Repository;

use App\Entity\Clubes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Clubes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Clubes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Clubes[]    findAll()
 * @method Clubes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClubesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Clubes::class);
    }

    /**
     * @throws OptimisticLockException|ORMException
     */
    public function saveClub($club, $request): Clubes
    {
        $club->setNombre($request->nombre);
        $club->setEscudo($request->escudo);
        $club->setLimiteJugadores($request->limiteJugadores);
        $club->setLimiteSalarial($request->limiteSalarial);
        $this->_em->persist($club);
        $this->_em->flush();

        return $club;
    }
}

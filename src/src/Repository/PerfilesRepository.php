<?php

namespace App\Repository;

use App\Entity\Perfiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Perfiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method Perfiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method Perfiles[]    findAll()
 * @method Perfiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PerfilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Perfiles::class);
    }

    /**
     * @throws OptimisticLockException|ORMException
     */
    public function saveProfile(Perfiles $profile, $request): Perfiles
    {
        $profile->setNombre(trim($request->nombre));
        $profile->setIsPagado($request->isPagado);
        $profile->setHasPosicion($request->hasPosicion);
        $profile->setHasLimitUser($request->hasLimitUser);
        $this->_em->persist($profile);
        $this->_em->flush();

        return $profile;
    }
}

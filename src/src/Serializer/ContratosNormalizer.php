<?php

namespace App\Serializer;

use App\Entity\Contratos;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ContratosNormalizer implements ContextAwareNormalizerInterface
{
    private $normalizer;

    public function __construct(
        ObjectNormalizer $normalizer
    ) {
        $this->normalizer = $normalizer;
    }

    public function normalize($contrato, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($contrato, $format, $context);
        if ($contrato->getClub()) {
            $data['club'] = $contrato->getClub()->getId();
        }
        if ($contrato->getUser()) {
            $data['nombre'] = $contrato->getUser()->getNombre();
            $data['userId'] = $contrato->getUser()->getId();
            $data['correo'] = $contrato->getUser()->getEmail();
            if ($contrato->getUser()->getPerfil()) {
                $data['perfil'] = $contrato->getUser()->getPerfil()->getNombre();
                $data['perfilId'] = $contrato->getUser()->getPerfil()->getId();
                $data['hasLimitUser'] = $contrato->getUser()->getPerfil()->getHasLimitUser();
                $data['hasPosicion'] = $contrato->getUser()->getPerfil()->getHasPosicion();
                $data['isPagado'] = $contrato->getUser()->getPerfil()->getIsPagado();
            }
            if ($contrato->getUser()->getPosicion()) {
                $data['posicion'] = $contrato->getUser()->getPosicion()->getNombre();
            }
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof Contratos;
    }
}

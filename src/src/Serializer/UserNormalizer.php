<?php

namespace App\Serializer;

use App\Entity\User;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserNormalizer implements ContextAwareNormalizerInterface
{
    private $normalizer;

    public function __construct(
        ObjectNormalizer $normalizer
    ) {
        $this->normalizer = $normalizer;
    }

    public function normalize($user, $format = null, array $context = [])
    {
        /*if($user->getPerfil()){
            $data['perfil'] = $user->getPerfil()->getNombre();
        }
        if($user->getPosicion()){
            $data['posicion'] = $user->getPosicion()->getNombre();
        }
        return $this->normalizer->normalize($user, $format, $context);*/
        $data = $this->normalizer->normalize($user, $format, $context);
        if ($user->getFechaNacimiento()) {
            $data['fechaNacimiento'] = $user->getFechaNacimiento()->format('d/m/Y');
        }
        if ($user->getPosicion()) {
            $data['posicion'] = $user->getPosicion()->getNombre();
            $data['posicionId'] = $user->getPosicion()->getId();
        }
        if ($user->getPerfil()) {
            $data['perfil'] = $user->getPerfil()->getNombre();
            $data['perfilId'] = $user->getPerfil()->getId();
            $data['hasLimitUser'] = $user->getPerfil()->getHasLimitUser();
            $data['hasPosicion'] = $user->getPerfil()->getHasPosicion();
            $data['isPagado'] = $user->getPerfil()->getIsPagado();
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof User;
    }
}

<?php

namespace App\Services;

use App\Entity\Clubes;
use App\Repository\ClubesRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\RequestStack;

class ClubesServices
{
    /**
     * @var RequestStack
     */
    private $request;
    /**
     * @var ClubesRepository
     */
    private $clubesRepository;

    public function __construct(RequestStack $requestStack, ClubesRepository $clubesRepository)
    {
        $this->request = json_decode($requestStack->getCurrentRequest()->getContent());
        $this->clubesRepository = $clubesRepository;
    }

    /**
     * @throws OptimisticLockException|ORMException
     */
    public function clubManagement(): Clubes
    {
        if ($this->request->id) {
            $club = $this->clubesRepository->find($this->request->id);
        } else {
            $club = $this->clubesRepository->findOneBy(['nombre' => $this->request->nombre]);
        }
        if (!$club) {
            $club = new Clubes();
        }

        return $this->clubesRepository->saveClub($club, $this->request);
    }
}

<?php

namespace App\Services;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserServices
{
    /**
     * @var RequestStack
     */
    private $request;
    /**
     * @var UserPasswordHasher
     */
    private $hasher;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(RequestStack $requestStack, UserPasswordHasherInterface $hasher, UserRepository $userRepository)
    {
        $this->request = json_decode($requestStack->getCurrentRequest()->getContent());
        $this->hasher = $hasher;
        $this->userRepository = $userRepository;
    }

    public function userManagement(): User
    {
        if ($this->request->id) {
            $user = $this->userRepository->find($this->request->id);
        } else {
            $user = $this->userRepository->findOneBy(['nombre' => $this->request->nombre]);
        }
        $tipo = 1;
        if (!$user) {
            $tipo = 2;
            $user = new User();
            $user->setPassword($this->hasher->hashPassword($user, 'pass123'));
        }

        return $this->userRepository->saveUser($user, $this->request, $tipo);
    }
}

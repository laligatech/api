<?php

namespace App\Services;

use App\Entity\Perfiles;
use App\Repository\PerfilesRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\RequestStack;

class PerfilesServices
{
    /**
     * @var RequestStack
     */
    private $request;
    /**
     * @var PerfilesRepository
     */
    private $perfilesRepository;

    public function __construct(RequestStack $requestStack, PerfilesRepository $perfilesRepository)
    {
        $this->request = json_decode($requestStack->getCurrentRequest()->getContent());
        $this->perfilesRepository = $perfilesRepository;
    }

    /**
     * @throws OptimisticLockException|ORMException
     */
    public function profileManagement(): Perfiles
    {
        if ($this->request->id) {
            $profile = $this->perfilesRepository->find($this->request->id);
        } else {
            $profile = $this->perfilesRepository->findOneBy(['nombre' => $this->request->nombre]);
        }
        if (!$profile) {
            $profile = new Perfiles();
        }

        return $this->perfilesRepository->saveProfile($profile, $this->request);
    }
}

<?php

namespace App\Services\Correos;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class CorreoService
{
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var ContainerInterface
     */
    private $sContainer;

    public function __construct(MailerInterface $mailer, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->sContainer = $container;
    }

    public function notificacion($arr): array
    {
        $email = (new Email())
            ->from($this->sContainer->getParameter('app.admin_email'))
            ->to($arr['destino'])
            ->bcc('desarrollo@heriobbdev.es')
            ->subject($arr['subject'])
            ->html($arr['body']);
        try {
            $this->mailer->send($email);

            return ['res' => 1, 'text' => 'Enviado'];
        } catch (TransportExceptionInterface | \Exception $e) {
            return ['res' => 0, 'text' => $e->getMessage()];
        }
    }
}

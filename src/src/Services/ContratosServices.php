<?php

namespace App\Services;

use App\Entity\Contratos;
use App\Repository\ContratosRepository;
use App\Services\Correos\CorreoService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\RequestStack;

class ContratosServices
{
    /**
     * @var RequestStack
     */
    private $request;
    /**
     * @var ContratosRepository
     */
    private $contratosRepository;
    /**
     * @var CorreoService
     */
    private $correoService;

    public function __construct(RequestStack $requestStack, ContratosRepository $contratosRepository, CorreoService $correoService)
    {
        $this->request = json_decode($requestStack->getCurrentRequest()->getContent());
        $this->contratosRepository = $contratosRepository;
        $this->correoService = $correoService;
    }

    /**
     * @throws OptimisticLockException|ORMException
     * @throws \Exception
     */
    public function contractManagement(): Contratos
    {
        //solo usuarios sin contrato activo
        if ($this->contratosRepository->findOneBy(['user' => $this->request->userId, 'activo' => true])) {
            if (($this->request->id > 0 && $this->request->activo) || ($this->request->activo)) {
                throw new \Exception(sprintf('Este "%s" tiene un contrato activo!', $this->request->perfil));
            }
        }

//        limite Jugadores
        if ($this->request->hasLimitUser) {
            $limite = $this->contratosRepository->limiteContratosPorUsuario($this->request->club);
            if (($limite && count($limite) >= $this->request->limiteJugadores) && ($this->request->activo)) {
                throw new \Exception(sprintf('Ya alcanzo el limite de "%s" para este club!', $this->request->perfil));
            }
            //limite entrenador
            if ($this->contratosRepository->limiteEntrenador($this->request)) {
                throw new \Exception('Ya alcanzo el limite de entrenadores para este club!');
            }
        }

        $contract = null;
        if ($this->request->id) {
            $contract = $this->contratosRepository->find($this->request->id);
        }
        if (!$contract) {
            $contract = new Contratos();
        }
        $contrato = $this->contratosRepository->saveContract($contract, $this->request);
        if ($this->request->activo) {
            $subject = 'Bienvenido a La Liga!';
            $body = '<h2>¡Enhorabuena! '.$contrato->getClub()->getNombre().' te ha dado de alta.</h2> <br> <h4>Saludos!</h4> <br><h5>Este correo no admite respuestas.</h5>';
        } else {
            $subject = 'En La Liga te echaremos de menos!';
            $body = '<h2>¡Este no es un Adios! '.$contrato->getClub()->getNombre().' te ha dado de baja.</h2> <br><h3>Esperamos que vuelvas pronto.</h3><br> <h4>Saludos!</h4> <br><h5>Este correo no admite respuestas.</h5>';
        }
        $this->correoService->notificacion([
            'destino' => $contrato->getUser()->getEmail(),
            'subject' => $subject,
            'body' => $body,
        ]);

        return $contrato;
    }
}

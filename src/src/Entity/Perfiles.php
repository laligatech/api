<?php

namespace App\Entity;

use App\Repository\PerfilesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PerfilesRepository::class)
 */
class Perfiles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPagado;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasPosicion;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasLimitUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getIsPagado(): ?bool
    {
        return $this->isPagado;
    }

    public function setIsPagado(bool $isPagado): self
    {
        $this->isPagado = $isPagado;

        return $this;
    }

    public function getHasPosicion(): ?bool
    {
        return $this->hasPosicion;
    }

    public function setHasPosicion(bool $hasPosicion): self
    {
        $this->hasPosicion = $hasPosicion;

        return $this;
    }

    public function getHasLimitUser(): ?bool
    {
        return $this->hasLimitUser;
    }

    public function setHasLimitUser(bool $hasLimitUser): self
    {
        $this->hasLimitUser = $hasLimitUser;

        return $this;
    }
}

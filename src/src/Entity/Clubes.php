<?php

namespace App\Entity;

use App\Repository\ClubesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClubesRepository::class)
 */
class Clubes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $escudo;

    /**
     * @ORM\Column(type="bigint")
     */
    private $limiteJugadores;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $limiteSalarial;

    /**
     * @ORM\OneToMany(targetEntity=Contratos::class, mappedBy="club")
     */
    private $contratos;

    public function __construct()
    {
        $this->contratos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEscudo(): ?string
    {
        return $this->escudo;
    }

    public function setEscudo(string $escudo): self
    {
        $this->escudo = $escudo;

        return $this;
    }

    public function getLimiteJugadores(): ?string
    {
        return $this->limiteJugadores;
    }

    public function setLimiteJugadores(string $limiteJugadores): self
    {
        $this->limiteJugadores = $limiteJugadores;

        return $this;
    }

    public function getLimiteSalarial(): ?string
    {
        return $this->limiteSalarial;
    }

    public function setLimiteSalarial(string $limiteSalarial): self
    {
        $this->limiteSalarial = $limiteSalarial;

        return $this;
    }

    /**
     * @return Collection|Contratos[]
     */
    public function getContratos(): Collection
    {
        return $this->contratos;
    }

    public function addContrato(Contratos $contrato): self
    {
        if (!$this->contratos->contains($contrato)) {
            $this->contratos[] = $contrato;
            $contrato->setClub($this);
        }

        return $this;
    }

    public function removeContrato(Contratos $contrato): self
    {
        if ($this->contratos->removeElement($contrato)) {
            // set the owning side to null (unless already changed)
            if ($contrato->getClub() === $this) {
                $contrato->setClub(null);
            }
        }

        return $this;
    }
}

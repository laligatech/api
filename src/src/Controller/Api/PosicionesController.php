<?php

namespace App\Controller\Api;

use App\Repository\PosicionesRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class PosicionesController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/posiciones/list")
     * @Rest\View(serializerGroups={"posiciones"}, serializerEnableMaxDepthChecks=true)
     */
    public function listPosiciones(PosicionesRepository $posicionesRepository): array
    {
        return $posicionesRepository->findAll();
    }
}

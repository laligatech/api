<?php

namespace App\Controller\Api;

use App\Repository\ContratosRepository;
use App\Services\ContratosServices;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class ContratosController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/contratos/list/{club}")
     * @Rest\View(serializerGroups={"contrato"}, serializerEnableMaxDepthChecks=true)
     */
    public function listPerfiles(ContratosRepository $contratosRepository, $club): array
    {
        return $contratosRepository->findBy(['club' => $club]);
    }

    /**
     * @Rest\Post(path="/contratos/management")
     * @Rest\View(serializerGroups={"contrato"}, serializerEnableMaxDepthChecks=true)
     */
    public function profileManagement(ContratosServices $contratosServices): \App\Entity\Contratos
    {
        return $contratosServices->contractManagement();
    }
}

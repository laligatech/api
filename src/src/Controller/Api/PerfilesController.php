<?php

namespace App\Controller\Api;

use App\Repository\PerfilesRepository;
use App\Services\PerfilesServices;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class PerfilesController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/perfiles/list")
     * @Rest\View(serializerGroups={"perfil"}, serializerEnableMaxDepthChecks=true)
     */
    public function listPerfiles(PerfilesRepository $perfilesRepository): array
    {
        return $perfilesRepository->findAll();
    }

    /**
     * @Rest\Post(path="/perfiles/management")
     * @Rest\View(serializerGroups={"perfil"}, serializerEnableMaxDepthChecks=true)
     */
    public function profileManagement(PerfilesServices $perfilesServices): \App\Entity\Perfiles
    {
        return $perfilesServices->profileManagement();
    }
}

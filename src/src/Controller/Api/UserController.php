<?php

namespace App\Controller\Api;

use App\Repository\UserRepository;
use App\Services\UserServices;
use Doctrine\DBAL\Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class UserController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/user/list")
     * @Rest\View(serializerGroups={"user"}, serializerEnableMaxDepthChecks=true)
     */
    public function listUsers(UserRepository $userRepository): array
    {
        return $userRepository->findAll();
    }

    /**
     * @Rest\Get(path="/user/list/{perfil}")
     * @Rest\View(serializerGroups={"user"}, serializerEnableMaxDepthChecks=true)
     *
     * @throws Exception
     */
    public function listUsersByPerfil(UserRepository $userRepository, $perfil): array
    {
//        return $userRepository->findBy(['perfil' => $perfil]);
        return $userRepository->usersByPerfilNotHired($perfil);
    }

    /**
     * @Rest\Post(path="/user/management")
     * @Rest\View(serializerGroups={"user"}, serializerEnableMaxDepthChecks=true)
     */
    public function userManagement(UserServices $jugadoresServices): \App\Entity\User
    {
        return $jugadoresServices->userManagement();
    }
}

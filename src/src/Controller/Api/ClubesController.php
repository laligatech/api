<?php

namespace App\Controller\Api;

use App\Repository\ClubesRepository;
use App\Services\ClubesServices;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class ClubesController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/clubes/list")
     * @Rest\View(serializerGroups={"clubes"}, serializerEnableMaxDepthChecks=true)
     */
    public function listclubes(ClubesRepository $clubesRepository): array
    {
        return $clubesRepository->findAll();
    }

    /**
     * @Rest\Post(path="/clubes/management")
     * @Rest\View(serializerGroups={"clubes"}, serializerEnableMaxDepthChecks=true)
     */
    public function clubManagement(ClubesServices $clubesServices): \App\Entity\Clubes
    {
        return $clubesServices->clubManagement();
    }
}
